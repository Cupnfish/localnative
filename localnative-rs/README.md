# localnative-rs

`cargo build`

# localnative-iced

### windows and mac

`cargo build`

### linux

Dependencies:
```bash
sudo apt install pkg-config libfreetype6-dev libfontconfig1-dev
```
and then `cargo build`

## License
[AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
